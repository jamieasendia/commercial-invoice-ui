﻿using System.Security.AccessControl;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct ConsigneeNameAndAddress
    {
        public string ConsigneeName { get; set; }
        public string ConsigneeAddressLine1 { get; set; }
        public string ConsigneeAddressLine2 { get; set; }
        public string ConsigneeCity { get; set; }
        public string ConsigneeState { get; set; }
        public string ConsigneePostcode { get; set; }
        public string ConsigneeCountry { get; set; }
        public string HtmlString =>string.Join("<br/>",ConsigneeName,ConsigneeAddressLine1,ConsigneeAddressLine2,ConsigneeCity,ConsigneeState,ConsigneePostcode,ConsigneeCountry);
      
    }

  
}