﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Enums
{
    public enum AUTH_ERROR_CODE
    {
        AUTH_ERROR_USERNAME_IN_USE = 1000,
        AUTH_ERROR_DB_ERROR,
        AUTH_ERROR_UNEXPECTED,
        AUTH_ERROR_INVALID_CREDENTIALS,
        AUTH_ERROR_INVALID_NOT_AUTHORISED,
        AUTH_ERROR_ACCOUNT_LOCKED,
        AUTH_ERROR_ACCOUNT_EXPIRED
    }
}
