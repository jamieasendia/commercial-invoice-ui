﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct CommercialInvoiceResult
    {
        public int ParcelId { get; set; }
        public string FinalMileReference { get; set; }
        public DateTime DateTime { get; set; }
        public string HouseAwb { get; set; }
        public int Pieces { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public Decimal Weight { get; set; }
        public string Country { get; set; }
        public string ParcelCode { get; set; }
        public int ParcelItemId { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Composition { get; set; }
        public string Origin { get; set; }
        public string Currency { get; set; }
        public decimal Value { get; set; }
        public string EccnRef { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddressLine1 { get; set; }
        public string RetailerAddressLine2 { get; set; }
        public string RetailerCity { get; set; }
        public string RetailerState { get; set; }
        public string RetailerPostcode { get; set; }
        public string RetailerCountry { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneeAddressLine1 { get; set; }
        public string ConsigneeAddressLine2 { get; set; }
        public string ConsigneeCity { get; set; }
        public string ConsigneeState { get; set; }
        public string ConsigneePostcode { get; set; }
        public string ConsigneeCountry { get; set; }

    }
}
