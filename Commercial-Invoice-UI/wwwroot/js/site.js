var $postedData;
var $countries;
const TransportType =
{
    ROAD:1,
    AIR:2
}


function loadingTemplate(message) {
    return '<i class="fa fa-spinner fa-spin fa-fw fa-2x"></i>';
}

function post(data, url) {
    var f = document.getElementById('FakeForm');
    $(f).attr("action", url);
    f.data.value = data;
    window.open('', '_new');
    f.submit();
} 

function storeQueryParams() {

    var sendersName, receiversName, dateFrom, dateTo, country;
    var transportType = $('#TransportType').val();//transportType;//$('input[name="roadAirCheckbox"]:checked').val();
    var  mawbNumber = $('#mawbSearchText').val();
    if ('Air' == transportType) {
        sendersName = $('#airSendersNameSearchText').val();
        receiversName = $('#airReceiverNameSearchText').val();
        dateFrom = moment.utc($('#airDateFrom').val(), 'DD-MM-YYYY').toISOString();
        dateTo = moment.utc($('#airDateTo').val(), 'DD-MM-YYYY').toISOString();
        country = Array.from($("#airCountrySelect option:selected")).map(o=>o.value);

        
    }
    if ('Road' == transportType) {
        
        sendersName = $('#roadSendersNameSearchText').val();
        receiversName = $('#roadReceiverNameSearchText').val();
        dateFrom = moment.utc($('#roadDateFrom').val(), 'DD-MM-YYYY').toISOString();
        dateTo = moment.utc($('#roadDateTo').val(), 'DD-MM-YYYY').toISOString();
        country = Array.from($("#roadCountrySelect option:selected")).map(o=>o.value);
    }


    
    $postedData = {
        ParcelConsignmentOrderReference:$('#parcelSearchText').val(),
        UseAdvancedSearch:$('#UseAdvancedSearch').val(),
        VehicleRegistration:$('#vehicleRegistrationSearchText').val(),
            TrailerNumber:$('#trailerSearchText').val(),
        SealNumber:$('#sealSearchText').val(),
        TransportType : transportType,
        SendersName:sendersName,
        ReceiversName:receiversName,
        MawbNumber:mawbNumber,
        DateFrom:dateFrom,
        DateTo:dateTo,
        CountryIds:country,
        Airline:$('#airlineSearchText').val(),
        FlightNumber:$('#flightSearchText').val()
    }


}

function postQueryParams(params) {

    //var form = $('#searchForm');
    //var token = $('input[name="__RequestVerificationToken"]',form).val();

    params.ParcelConsignmentOrderReference = $('#parcelSearchText').val();
    params.UseAdvancedSearch = $('#UseAdvancedSearch').val();
    if (params.UseAdvancedSearch == undefined) {
        params.UseAdvancedSearch = 'false';
    }

    //var transportType = //$('input[name="roadAirCheckbox"]:checked').val();
    var transportType = $('#TransportType').val();
    params.TransportType = transportType;//transportType;
    if ('Air' == transportType) {
        
        params.SendersName = $('#airSendersNameSearchText').val();
        params.ReceiversName = $('#airReceiverNameSearchText').val();
        params.MawbNumber = $('#mawbSearchText').val();
        params.DateFrom = moment.utc($('#airDateFrom').val(), 'DD-MM-YYYY').toISOString();
        params.DateTo = moment.utc($('#airDateTo').val(), 'DD-MM-YYYY').toISOString();
        params.CountryIds = Array.from($("#airCountrySelect option:selected")).map(o=>o.value);
        params.Airline = $('#airlineSearchText').val(), 
        params.FlightNumber = $('#flightSearchText').val();
    }
    if ('Road' == transportType) {
        
        params.SendersName = $('#roadSendersNameSearchText').val();
        params.ReceiversName = $('#roadReceiverNameSearchText').val();
        params.DateFrom = moment.utc($('#roadDateFrom').val(), 'DD-MM-YYYY').toISOString();
        params.DateTo = moment.utc($('#roadDateTo').val(), 'DD-MM-YYYY').toISOString();
        params.CountryIds = Array.from($("#roadCountrySelect option:selected")).map(o=>o.value);
        params.VehicleRegistration = $('#vehicleRegistrationSearchText').val();
        params.TrailerNumber = $('#trailerSearchText').val();
        params.SealNumber = $('#sealSearchText').val();
    }

    return $postedData = params;
    //return {
    //    __RequestVerificationToken: token,
    //    searchQuery: $postedData
    //}
    //return params; // body data

}

function rowStyle(row, index) {
    return {
        css: {
            cursor: 'pointer'
        }
    }
}

window.ajaxOptions = {
    beforeSend: function (xhr) {
        xhr.setRequestHeader("XSRF-TOKEN",
            $('input:hidden[name="__RequestVerificationToken"]').val());
    }
}

function operateFormatter(value, row, index) {
    return [
        '<a class="viewInvoice" href="javascript:void(0)" title="View">',
        '<i class="fa fa-file-pdf-o"></i>',
        '</a>  '
    ].join('');
}

function dateFormatter(value, row, index) {
    return moment(value).format('DD-MM-YYYY');
}


$.fn.clearErrors = function () {
    $(this).each(function() {
        $(this).find(".field-validation-error").empty();
        //$(this).trigger('reset.unobtrusiveValidation');
       
        $(this).validate().resetForm(true);
    });
};

function resetForm() {
    event.preventDefault();
    $('#tableWrapper').fadeOut(200);
    
    $("#searchForm")[0].reset();
    $("#searchForm").clearErrors();
    $('#airCountrySelect').val(null).trigger('change');
    $('#roadCountrySelect').val(null).trigger('change');
    //document.getElementById("searchForm").reset();
   
    

    $('#airSendersNameSearchText').autoComplete('set', null);
    $('#roadSendersNameSearchText').autoComplete('set', null);
    rebindControls();


  /*  var fromDate = moment().subtract(2, 'weeks').format('DD-MM-YYYY');
    var toDate = moment().format('DD-MM-YYYY');
    $('#roadDateFrom').data('daterangepicker').setStartDate(fromDate);
    $('#airDateFrom').data('daterangepicker').setStartDate(fromDate);

    $('#roadDateTo').data('daterangepicker').setStartDate(toDate);
    $('#airDateTo').data('daterangepicker').setStartDate(toDate);*/
    


  //  $('#airDateFrom').data('daterangepicker').setStartDate(moment().subtract(2, 'weeks').format("DD/MM/YYYY"));
}

