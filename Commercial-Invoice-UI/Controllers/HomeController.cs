﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Helpers;
using Commercial_Invoice_CodeLib.Model;
using Commercial_Invoice_CodeLib.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Commercial_Invoice_UI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wkhtmltopdf.NetCore;
using Wkhtmltopdf.NetCore.Options;

namespace Commercial_Invoice_UI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ISearchService _searchService;
        private readonly IMemoryCache _memoryCache;
        

        public HomeController(ILogger<HomeController> logger, ISearchService searchService, IMemoryCache memoryCache)
        {
            _logger = logger;
            _searchService = searchService;
            _memoryCache = memoryCache;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Home/Search")]
        public async Task<SearchViewModel> Search(AdvancedQuery searchQuery)
        {
            _logger.LogInformation($"Search request started {GetUserName()} - {JsonConvert.SerializeObject(searchQuery)}");
            SearchViewModel searchResult = new SearchViewModel();
            if (!ModelState.IsValid)
            {
                return searchResult;

            }
            if (searchQuery is null) return searchResult;
            if (!searchQuery.UseAdvancedSearch) {searchResult = await _searchService.RunSimpleSearch(searchQuery,GetAllowedBranchIds());}
            else {searchResult = await _searchService.RunAdvancedSearch(searchQuery,GetAllowedBranchIds());}
            _logger.LogInformation($"Search request complete {GetUserName()}");
            return searchResult;
        }


        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Home/ValidateParameters")]
        
        public  IActionResult ValidateParameters(AdvancedQuery searchQuery)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_SearchBarViewPartial",searchQuery);
            }

            return Json(new {success = true});
        }

 
        


   
    }
}