﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Configuration;
using Commercial_Invoice_CodeLib.Model;
using Dapper;
using DapperQueryBuilder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Commercial_Invoice_CodeLib.Service
{
    public class LookupService : ServiceBase<LookupService>,ILookupService
    {
        
        public async Task<IEnumerable<string>> GetParentRetailerName(string query)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select top 5 parentretailername from
dbo.parentretailer with (nolock)
		WHERE 1=1"); 
            if (!String.IsNullOrEmpty(query))
            {
                sql.AppendLine($" AND parentretailername like {query + "%"}");
            }

            sql.AppendLine($" ORDER BY PARENTRETAILERNAME ASC");

            var result = await sql.QueryAsync<string>();
 
            return result;
        }

        public async Task<IEnumerable<string>> GetRetailerName(string query)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select top 5 retailername from
dbo.retailer with (nolock)
		WHERE 1=1"); 
            if (!String.IsNullOrEmpty(query))
            {
                sql.AppendLine($" AND retailername like {query + "%"}");
            }

            sql.AppendLine($" ORDER BY RETAILERNAME ASC");

            var result = await sql.QueryAsync<string>();
 
            return result;
        }


        public async Task<IEnumerable<string>> GetAllRetailerNames()
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct retailername from
dbo.retailer with (nolock)
		WHERE 1=1"); 
        

            var result = await sql.QueryAsync<string>();
 
            return result;
        }
        
        public async Task<IEnumerable<string>> GetAllParentRetailerNames()
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct parentretailername from
dbo.parentretailer with (nolock)
		WHERE 1=1"); 
        

            var result = await sql.QueryAsync<string>();
 
            return result;
        }

        public async Task<IEnumerable<Country>> GetAllCountries()
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select c.Countryid, c.CountryName, c.CountryCode, c.ISO3CountryCode from dbo.Country c
inner join dbo.CountryConfiguration cc on c.CountryID = cc.CountryID
where cc.Enabled = 1
and c.CountryCode <> '--' order by countryname"); 
        

            var result = await sql.QueryAsync<Country>();
 
            return result;
        }

        public async Task<IEnumerable<string>> GetBranchNames(IEnumerable<int> branchIds)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($"select Branch from dbo.Branch where BranchID in {branchIds}");

            var cacheName = JsonConvert.SerializeObject(new {LookupType = "BranchNamesByIds", branchIds});

            IEnumerable<string> result;
            if (this._memoryCache.TryGetValue(cacheName, out IEnumerable<string> cacheEntry))
            {
                result = cacheEntry;
            }
            else
            {
                result = await sql.QueryAsync<string>();
                this._memoryCache.Set(cacheName, result, _memoryCacheEntryOptions);
            }

            return result;
        }

        public async Task<string> GetCarrierConsignmentCode(int parcelId)
        {
            throw new NotImplementedException();
        }

        public LookupService(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings, IOptionsMonitor<AppSettings> xAppSettings, ILogger<LookupService> logger, IMemoryCache memoryCache) 
            : base(xConnectionSettings,xAppSettings,logger,memoryCache)
        {
        }
    }
}