﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Http;

namespace Commercial_Invoice_CodeLib.Model
{
    public record AdvancedQuery:SearchQuery, IValidatableObject
    {
        private DateTime? _dateTo;

        public bool UseAdvancedSearch { get; set; } = false;

        public string VehicleRegistration { get; set; }
        public string TrailerNumber { get; set; }
        public string SealNumber { get; set; }
        
        public IEnumerable<int> CountryIds { get; set; }
        public string SendersName { get; set; }
        public string ReceiversName { get; set; }
        
        
        public DateTime? DateFrom { get; set; }
        
        public DateTime? DateTo
        {
            get => _dateTo;
            set
            {
                if (value.HasValue)
                {
                    _dateTo = value.Value.Date.AddDays(1).AddSeconds(-1);
                }
                
            }
        }


        //Additional properties to cover air and road seperate
        public TransportType TransportType { get; set; } = TransportType.Road; //Road will be default type
    
        public string MawbNumber { get; set; }
        public string Airline { get; set; }
        public string FlightNumber { get; set; }



      public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
      {
            var searchProp = new[]{"ParcelConsignmentOrderReference"};
            var dateFrom = new[] {"DateFrom"};
            var dateTo = new[] {"DateTo"};
            var transportType = new[] {"TransportType"};
            var mawbNumber = new[] {"MawbNumber"};
            var vehicleRegistration = new[] {"VehicleRegistration"};

       //   var results = new List<ValidationResult>();
          if (!UseAdvancedSearch && String.IsNullOrEmpty(ParcelConsignmentOrderReference))
          {
              yield return new ValidationResult("Search query cannot be empty", searchProp);
          }

          if (!Enum.IsDefined(TransportType))
          {
              yield return new ValidationResult("Transport type needs to be set", transportType);
          }



          if (UseAdvancedSearch)
          {
              switch (TransportType)
              {
                  case TransportType.Air:
                      if (string.IsNullOrEmpty(MawbNumber) && string.IsNullOrEmpty(ParcelConsignmentOrderReference))
                      {
                          yield return new ValidationResult("MAWB number cannot be empty", mawbNumber);
                      }
                      break;
                  case TransportType.Road:
                      if (string.IsNullOrEmpty(VehicleRegistration) && string.IsNullOrEmpty(ParcelConsignmentOrderReference))
                      {
                          yield return new ValidationResult("Vehicle Registration number cannot be empty", vehicleRegistration);
                      }
                      break;
              }
          }

      }
    }



    

    public enum TransportType
    {
            Road=1,
            Air=2

    }
}