﻿using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface IEmailService
    {
        Task Send(string from, string to, string subject, string message);
    }
}