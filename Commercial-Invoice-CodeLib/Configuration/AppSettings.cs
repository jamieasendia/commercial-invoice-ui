﻿namespace Commercial_Invoice_CodeLib.Configuration
{
    public record AppSettings
    {
        public string SystemApplicationName { get; set; }
        public int DefaultPageSize { get; set; }
        public bool ShowBranches { get; set; }
    }
}
