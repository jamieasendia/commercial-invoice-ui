﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: IAuntentcationService.cs
// Date:     17/03/2021
// Author:   jamie.buchan
#endregion

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Authentication;
using System.Threading.Tasks;
using AsendiaShared.DTO.Entities;
using AsendiaShared.DTO.Enums;
using Commercial_Invoice_CodeLib.Configuration;
using Commercial_Invoice_CodeLib.Enums;
using Dapper;
using DapperQueryBuilder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Commercial_Invoice_CodeLib.Service
{
    public class AuthService :ServiceBase<AuthService>, IAuthService
    {
        public AuthService(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings, IOptionsMonitor<AppSettings> xAppSettings, ILogger<AuthService> logger, IMemoryCache memoryCache) 
        : base(xConnectionSettings,xAppSettings,logger,memoryCache)
        {
        }


        public async Task<User> GetUserByLogin(string xUsername)
        {
            User user = null;

            await using var connection = new SqlConnection(_connectionString);
            
                string sql = @"SELECT u.UserID, u.DisplayName, u.Username, u.Password, (u.IsLocked+1) Status,
                u.Email, 1 as 'AllowWrite', 1 as 'AllowDelete', 1 as 'AllowExport'
                from dbo.[User] u
                    INNER JOIN dbo.User_UserGroup uug with (nolock) on u.UserID = uug.UserID
                inner join dbo.UserGroup ug with (nolock) on uug.UserGroupID = ug.UserGroupID
                inner join UserManagement.UserGroup_SystemApplication usa with (nolock) on  ug.UserGroupID = usa.UserGroupID
                inner join UserManagement.SystemApplication sa with (nolock) on sa.SystemApplicationID = usa.SystemApplicationID
                where sa.SystemApplicationName = @ApplicationName and 
                (u.username = @Username  OR u.email = @Username) ";

                user = await connection.QuerySingleOrDefaultAsync<User>(sql, new { Username = xUsername, ApplicationName = _systemApplicationName });
            
                if (user != null)
                {
                    if (user.Status == USER_STATUS.VALID)
                    {
                        user.UserGroups = (await GetAssociatedUserGroups(user.UserID)).AsList();
                        user.Branches = (await GetAssociatedBranches(user.UserID)).AsList();
                    }
             

                }
            

            return user;
        }

        public async Task<User> GetUserByEmail(string xEmailAddress)
        {
            User user = null;

            await using var connection = new SqlConnection(_connectionString);
            
            string sql = @"SELECT u.UserID, u.DisplayName, u.Username, u.Password, (u.IsLocked+1) Status,
                u.Email, 1 as 'AllowWrite', 1 as 'AllowDelete', 1 as 'AllowExport'
                from dbo.[User] u
                    INNER JOIN dbo.User_UserGroup uug with (nolock) on u.UserID = uug.UserID
                inner join dbo.UserGroup ug with (nolock) on uug.UserGroupID = ug.UserGroupID
                inner join UserManagement.UserGroup_SystemApplication usa with (nolock) on  ug.UserGroupID = usa.UserGroupID
                inner join UserManagement.SystemApplication sa with (nolock) on sa.SystemApplicationID = usa.SystemApplicationID
                where sa.SystemApplicationName = @ApplicationName and 
                (u.emailaddress = @EmailAddress ) ";

            user = await connection.QuerySingleOrDefaultAsync<User>(sql, new { EmailAddress = xEmailAddress, ApplicationName = _systemApplicationName });

            return user;
        }

        public async Task<bool> TryResetPassword(string emailAddress)
        {
            var user = await GetUserByLogin(emailAddress);


            await SendResetPasswordRequest("", user);
            return true;
        }

        private async Task SendResetPasswordRequest(string xResetToken, User xUser)
        {
            
        }

        private async Task<IEnumerable<UserGroup>> GetAssociatedUserGroups(int xUserID)
        {
            IEnumerable<UserGroup> usergroups = new List<UserGroup>();

            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
                string sql = "SELECT ug.UserGroupID, ug.UserGroup AS 'UserGroupName' ";
                sql += "FROM dbo.[User_UserGroup] uug ";
                sql += "JOIN dbo.[UserGroup] ug ON uug.UserGroupID = ug.UserGroupID ";
                sql += "WHERE uug.UserID = @UserID;";

                usergroups = await connection.QueryAsync<UserGroup>(sql, new { UserID = xUserID });

            return usergroups;
        }

        private async Task<IEnumerable<Branch>> GetAssociatedBranches(int xUserID)
        {
            IEnumerable<Branch> branches = new List<Branch>();

            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
                string sql = "SELECT b.BranchID, b.BranchName AS 'Name', b.Branch AS 'Code' ";
                sql += "FROM dbo.[User_Branch] ub ";
                sql += "JOIN dbo.[Branch] b ON b.BranchID = ub.BranchID ";
                sql += "WHERE ub.UserID = @UserID;";

                branches = await connection.QueryAsync<Branch>(sql, new { UserID = xUserID });
                return branches;
        }

    }
}