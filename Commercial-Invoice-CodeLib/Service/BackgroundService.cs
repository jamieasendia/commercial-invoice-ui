﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: BackgroundService.cs
// Date:     04/03/2021
// Author:   jamie.buchan
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Hosting;

namespace Commercial_Invoice_CodeLib.Service
{
    public abstract class BackgroundService:IHostedService,IDisposable
    {
        protected readonly Random _random = new Random(10);
        private Task task;
        private readonly CancellationTokenSource _cancellationTokens = new CancellationTokenSource();


        protected abstract Task ExecuteAsync(CancellationToken stoppingToken);

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            this.task = this.ExecuteAsync(this._cancellationTokens.Token);
            return this.task.IsCompleted ? this.task : Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            if (this.task != null)
            {
                try
                {
                    this._cancellationTokens.Cancel();
                }
                finally
                {
                    await Task.WhenAny(task, Task.Delay(Timeout.Infinite, cancellationToken));
                }
            }
        }

        public virtual void Dispose()
        {
            this._cancellationTokens.Cancel();
        }
    }


    public class RetailerRefreshService : BackgroundService
    {
        private IMemoryCache _memoryCache;
        private readonly ILookupService _lookupService;
        private const string CACHENAME = "RETAILERS";

        public RetailerRefreshService(IMemoryCache memoryCache, ILookupService lookupService)
        {
            this._memoryCache = memoryCache;
            _lookupService = lookupService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await RefreshData();
                await Task.Delay(1000*3500, stoppingToken);
            }
        }

        private async Task RefreshData()
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromHours(24));


            if (!this._memoryCache.TryGetValue(CACHENAME, out IEnumerable<string> cacheEntry))
            {
                //cacheEntry = new List<string>();


                cacheEntry = (await _lookupService.GetAllRetailerNames()).OrderBy(s => s).ToList();
                this._memoryCache.Set(CACHENAME, cacheEntry, cacheEntryOptions);
            }


        }
    }


    public class ParentRetailerRefreshService : BackgroundService
    {
        private IMemoryCache _memoryCache;
        private readonly ILookupService _lookupService;
        private const string CACHENAME = "PARENTRETAILERS";

        public ParentRetailerRefreshService(IMemoryCache memoryCache, ILookupService lookupService)
        {
            this._memoryCache = memoryCache;
            _lookupService = lookupService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await RefreshData();
                await Task.Delay(1000*3600, stoppingToken);
            }
        }

        private async Task RefreshData()
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromHours(24));


            if (!this._memoryCache.TryGetValue(CACHENAME, out IEnumerable<string> cacheEntry))
            {
                //cacheEntry = new List<string>();

                cacheEntry = (await _lookupService.GetAllParentRetailerNames()).OrderBy(s => s).ToList();
                this._memoryCache.Set(CACHENAME, cacheEntry, cacheEntryOptions);
            }


        }

        
    }

    public class CountryRefreshService : BackgroundService
    {
        private IMemoryCache _memoryCache;
        private readonly ILookupService _lookupService;
        private const string CACHENAME = "COUNTRIES";

        public CountryRefreshService(IMemoryCache memoryCache, ILookupService lookupService)
        {
            _memoryCache = memoryCache;
            _lookupService = lookupService;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await RefreshData();
                await Task.Delay(1000*3700, stoppingToken);
            }
        }

        private async Task RefreshData()
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromHours(24));


            if (!this._memoryCache.TryGetValue(CACHENAME, out IEnumerable<Country> cacheEntry))
            {
                cacheEntry = (await _lookupService.GetAllCountries()).OrderBy(country => country.CountryName).ToList();
                this._memoryCache.Set(CACHENAME, cacheEntry, cacheEntryOptions);
            }


        }
        
    }
}