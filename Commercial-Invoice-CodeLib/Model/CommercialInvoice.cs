﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog.LayoutRenderers;

namespace Commercial_Invoice_CodeLib.Model
{
    public record CommercialInvoice
    {
        public int ParcelId { get; set; }
        public int ConsigneeId { get; set; }
        public int RetailerAddressId { get; set; }
        public string FinalMileReference { get; set; }
        public DateTime DateTime { get; set; }
        public string HouseAwb { get; set; }
        public int Pieces { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public string Measurements => $"{Width}x{Height}x{Length}";
        public decimal Weight { get; set; }
        public string Country { get; set; }
        public string ParcelCode { get; set; }
        public decimal Total { get; set; }
        
    }
}
