﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: SearchService.cs
// Date:     25/02/2021
// Author:   jamie.buchan
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Configuration;
using Commercial_Invoice_CodeLib.Model;
using Dapper;
using DapperQueryBuilder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

// ReSharper disable StringLiteralTypo

namespace Commercial_Invoice_CodeLib.Service
{
    public class SearchService : ServiceBase<SearchService>, ISearchService
    {
     
        private const string TOP_N_RESULTS_FOR_AUTOCOMPLETE = "5";

        private readonly FormattableString SEARCH_FROM = $@"FROM 
        IFOS.ScannedManifestTB sm (NOLOCK)
        left join IFOS.SplitsTB stb with (NOLOCK) on sm.MawbInformationID = stb.MawbInformationID
        left join IFOS.MawbInformationTB mtb with (NOLOCK) on sm.MawbInformationID = mtb.MawbInformationID
            JOIN Container cn (NOLOCK) ON cn.ContainerNo = sm.ItemScanned
            JOIN Parcel p (NOLOCK) ON p.ContainerID = cn.ContainerID
            JOIN Consignment c (NOLOCK) ON c.ConsignmentID = p.ConsignmentID
            JOIN ParentRetailer pr (NOLOCK) ON pr.ParentRetailerID = c.ParentRetailerID
            JOIN Consignee ce (NOLOCK) ON ce.ConsigneeID = c.ConsigneeID
            JOIN Country ct (NOLOCK) ON ct.CountryID = ce.CountryID
            WHERE 1=1";


 


        private readonly FormattableString GROUP_BY = $@" group by 
                                    p.ParcelId,
                                    p.OriginalRetailerBarcode,
		                            p.CarrierConsignmentCode,
		                            c.OrderNumber,
		                            ce.Name,
		                            pr.ParentRetailerName,
		                            ct.CountryName, 
		                            stb.VehicleRegistration ,
		                            stb.TrailerNumber,
		                            stb.VehicleSealNumber,
		                            stb.IMOReference,
                                    mtb.MawbNumber
";


        private static bool ShouldRunSimpleSearch(AdvancedQuery query)
        {
            if (string.IsNullOrEmpty(query.ParcelConsignmentOrderReference))return false;

            if (query.TransportType == TransportType.Road)
            {
                if (query.CountryIds != null && query.CountryIds.Any()) return false;
                if (!string.IsNullOrEmpty(query.ReceiversName)) return false;
                if (!string.IsNullOrEmpty(query.SendersName)) return false;
                if (!string.IsNullOrEmpty(query.VehicleRegistration)) return false;
                if (!string.IsNullOrEmpty(query.SealNumber)) return false;
                if (!string.IsNullOrEmpty(query.TrailerNumber)) return false;
                if (!string.IsNullOrEmpty(query.VehicleRegistration)) return false;
            }
            else
            {
                if (query.CountryIds != null && query.CountryIds.Any()) return false;
                if (!string.IsNullOrEmpty(query.ReceiversName)) return false;
                if (!string.IsNullOrEmpty(query.SendersName)) return false;
                if (!string.IsNullOrEmpty(query.MawbNumber)) return false;
                if (!string.IsNullOrEmpty(query.FlightNumber)) return false;

            }
            return true;
        }
  
        public async Task<SearchViewModel> RunSimpleSearch(SearchQuery query, IEnumerable<int> allowedBranches)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            

            var sql = connection.QueryBuilder($@"select distinct p.ParcelId, p.OriginalRetailerBarcode as RetailerParcelCode,
		p.CarrierConsignmentCode as CarrierConsignmentCode,
		c.OrderNumber as RetailerOrderReference,
		ce.Name as ReceiverName,
		pr.ParentRetailerName as SenderName,
		ct.CountryName as Country,
		stb.VehicleRegistration as VehicleRegistrationNumber,
		stb.TrailerNumber as TrailerNumber,
		stb.VehicleSealNumber as SealNumber,
		stb.IMOReference as ImoReference,
		COALESCE(min(stb.DateTimeRecordCreated),min(mtb.FlightDate)) as DepartureDate,
        mtb.MawbNumber as Mawb" 
    );
            sql.AppendLine(SEARCH_FROM);

            var sqlCount = connection.QueryBuilder($@"select count(distinct p.parcelid)");
            sqlCount.AppendLine(SEARCH_FROM);

            AddSimpleSearchParameters(query,sql);
            AddSimpleSearchParameters(query,sqlCount);
            AddBranchFilter(sql,allowedBranches);
            AddBranchFilter(sqlCount,allowedBranches);
            sql.AppendLine(GROUP_BY);
            //sqlCount.AppendLine(GROUP_BY);
            
		//Add in offset and fetch
       // sql.AppendLine($" ORDER by p.ParcelId DESC OFFSET {query.Offset} ROWS ");
       // sql.AppendLine($"  FETCH NEXT {query.Limit} ROWS ONLY");

        var searchResult = (await sql.QueryAsync<SearchResult>(commandTimeout:60)).ToList();

        var countResult = searchResult.Count();
       /* var cacheName = JsonConvert.SerializeObject(new {query, allowedBranches,QueryType="COUNT"});
        //var cacheName = sqlCount.Sql;
        int countResult = 0;
        if (this._memoryCache.TryGetValue(cacheName, out int cacheEntry))
        {
            countResult = cacheEntry;
            
        }
        else
        {
            countResult = await  sqlCount.QuerySingleAsync<int>();
            this._memoryCache.Set(cacheName, countResult, _memoryCacheEntryOptions);
        }*/
         //   var countResult = sqlCount.QuerySingleAsync<int>();
            
            var resultSearchViewModel = new SearchViewModel {Rows = searchResult, Total = countResult};
            return resultSearchViewModel;
        }

        public async Task<SearchViewModel> RunAdvancedSearch(AdvancedQuery query, IEnumerable<int> allowedBranches)
        {
            if (ShouldRunSimpleSearch(query)) return await RunSimpleSearch(query, allowedBranches);
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct p.ParcelId, p.OriginalRetailerBarcode as RetailerParcelCode,
		p.CarrierConsignmentCode as CarrierConsignmentCode,
		c.OrderNumber as RetailerOrderReference,
		ce.Name as ReceiverName,
		pr.ParentRetailerName as SenderName,
		ct.CountryName as Country,
		stb.VehicleRegistration as VehicleRegistrationNumber,
		stb.TrailerNumber as TrailerNumber,
		stb.VehicleSealNumber as SealNumber,
		stb.IMOReference as ImoReference,
		COALESCE(min(stb.DateTimeRecordCreated),min(mtb.FlightDate)) as DepartureDate,
        mtb.MawbNumber as Mawb"); 

            sql.AppendLine(SEARCH_FROM);
            
        var sqlCount = connection.QueryBuilder($@"select count(distinct p.parcelId)");
        sqlCount.AppendLine(SEARCH_FROM);

        switch (query.TransportType)
        {
            case TransportType.Road:
                AddRoadAdvancedSearchParameters(query, sql);
                AddRoadAdvancedSearchParameters(query, sqlCount);
                break;
            case TransportType.Air:
                AddAirAdvancedSearchParameters(query, sql);
                AddAirAdvancedSearchParameters(query, sqlCount);
                break;
        }

        AddBranchFilter(sql,allowedBranches);
        AddBranchFilter(sqlCount,allowedBranches);
        sql.AppendLine(GROUP_BY);

        sql.AppendLine($" ORDER by p.ParcelId DESC OFFSET {query.Offset} ROWS ");
        sql.AppendLine($"  FETCH NEXT {query.Limit} ROWS ONLY");

        var searchResult = sql.QueryAsync<SearchResult>();


        var cacheName = JsonConvert.SerializeObject(new {query, allowedBranches,QueryType="COUNT"});
        //var cacheName = sqlCount.Sql;
        int countResult = 0;
        if (this._memoryCache.TryGetValue(cacheName, out int cacheEntry))
        {
            countResult = cacheEntry;
            
        }
        else
        {
            countResult = await  sqlCount.QuerySingleAsync<int>();
            this._memoryCache.Set(cacheName, countResult, _memoryCacheEntryOptions);
        }
  
            var resultSearchViewModel = new SearchViewModel {Rows = (await searchResult).ToImmutableList(), Total = countResult};
            return resultSearchViewModel;
        }

        private void AddRoadAdvancedSearchParameters(AdvancedQuery query, QueryBuilder sql)
        {
                    AddSimpleSearchParameters(query,sql);
                    if (!String.IsNullOrEmpty(query.ReceiversName))
                    {
                        sql.AppendLine($" AND ce.Name = {query.ReceiversName }");
	        
                    }
        
                    if (!String.IsNullOrEmpty(query.SealNumber))
                    {
                        sql.AppendLine($" AND stb.VehicleSealNumber = {query.SealNumber}");
	       
                    }
        
                    if (!String.IsNullOrEmpty(query.SendersName))
                    {
                        sql.AppendLine($" AND pr.ParentRetailerName = {query.SendersName}");
	        
                    }

                    if (!String.IsNullOrEmpty(query.TrailerNumber))
                    {
                        sql.AppendLine($" AND stb.TrailerNumber = { query.TrailerNumber}");
           
                    }

                    if (!String.IsNullOrEmpty(query.VehicleRegistration))
                    {
                        sql.AppendLine($" AND stb.VehicleRegistration = {query.VehicleRegistration}");
          
                    }

                    if (query.DateFrom.HasValue && query.DateTo.HasValue)
                    {
                        if (query.DateFrom.Value.Date <= query.DateTo.Value.Date)
                        {
                            sql.AppendLine(
                                $" AND stb.DateTimeRecordCreated between {query.DateFrom.Value} and {query.DateTo.Value}");
              
                        }
                    }

                    if (query.CountryIds != null && query.CountryIds.Any())
                    {
                        sql.AppendLine($"AND ct.countryId in {query.CountryIds}");

                    }
                    //if (query.Country.HasValue)
                    //{
                    //    sql.AppendLine(
                   //         $" AND ct.CountryId = {query.Country.Value}");
                   // }
        }

        private  void AddAirAdvancedSearchParameters(AdvancedQuery query, QueryBuilder sql)
        {
                    

                    AddSimpleSearchParameters(query,sql);
                    if (!String.IsNullOrEmpty(query.ReceiversName))
                    {
                        sql.AppendLine($" AND ce.Name = {query.ReceiversName }");
	        
                    }
        
                    if (!String.IsNullOrEmpty(query.MawbNumber))
                    {
                        sql.AppendLine($" AND mtb.MawbNumber = {query.MawbNumber}");
	       
                    }
        
                    if (!String.IsNullOrEmpty(query.SendersName))
                    {
                        sql.AppendLine($" AND pr.ParentRetailerName = {query.SendersName}");
	        
                    }

                    if (!String.IsNullOrEmpty(query.FlightNumber))
                    {
                        sql.AppendLine($" AND mtb.flightnumber = { query.FlightNumber}");
           
                    }

                    if (!String.IsNullOrEmpty(query.Airline))
                    {
                        sql.AppendLine($" AND mtb.Airline = {query.Airline}");
          
                    }

                    if (query.DateFrom.HasValue && query.DateTo.HasValue)
                    {
                        if (query.DateFrom.Value.Date <= query.DateTo.Value.Date)
                        {
                            sql.AppendLine(
                                $" AND mtb.flightdate between {query.DateFrom.Value} and {query.DateTo.Value}");
              
                        }
                    }

                    if (query.CountryIds != null && query.CountryIds.Any())
                    {
                        sql.AppendLine($"AND ct.countryId in {query.CountryIds}");

                    }
                    //if (query.Country.HasValue)
                    //{
                    //    sql.AppendLine(
                   //         $" AND ct.CountryId = {query.Country.Value}");
                   // }
        }

        private void AddSimpleSearchParameters(SearchQuery query, QueryBuilder sql)
        {
            if (!String.IsNullOrEmpty(query.ParcelConsignmentOrderReference))
            {
                var countSplit = query.GetSplitParcelConsignmentOrderReferences.Count();
                var counter = 0;
                sql.AppendLine($" AND (");
                foreach (var searchSplitRef in query.GetSplitParcelConsignmentOrderReferences)
                {
                    if (counter>0) sql.AppendLine($" OR ");
                    var consignmentId = GetConsignmentIdForOrderNumber(searchSplitRef).Result;
                    sql.AppendLine(
                        $"  (p.MetapackTrackingCode = { searchSplitRef } OR p.CarrierConsignmentCode  = {searchSplitRef} OR p.consignmentid = {consignmentId} or p.OriginalRetailerBarcode = {searchSplitRef})");
                    counter++;
                }
                sql.AppendLine($" )");
                
            }
        }
        
        private async Task<int> GetConsignmentIdForOrderNumber(string  query)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct p.consignmentid
            from parcel p with (nolock) 
            inner join consignment c with (nolock) on p.consignmentid = c.consignmentid
            WHERE c.ordernumber = {query}                       
"           
            );

            var searchResult = await sql.QueryFirstOrDefaultAsync<int>();

            return searchResult;
            
            
         
        }
        

        private static void AddBranchFilter(QueryBuilder sql, IEnumerable<int> allowedBranches)
        {
            sql.AppendLine($" AND sm.BranchId in {allowedBranches}");
        }

        public async Task<IEnumerable<int>> GetParcelIdsFromSimpleSearch(SearchQuery query, IEnumerable<int> allowedBranches)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct p.ParcelId" 
            );
            sql.AppendLine(SEARCH_FROM);

            AddSimpleSearchParameters(query,sql);
            AddBranchFilter(sql,allowedBranches);
            var searchResult = await sql.QueryAsync<int>();

            return searchResult;
        }

        public async Task<IEnumerable<int>> GetParcelIdsFromAdvancedSearch(AdvancedQuery query, IEnumerable<int> allowedBranches)
        {
            if (ShouldRunSimpleSearch(query)) return await GetParcelIdsFromSimpleSearch(query, allowedBranches);
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var sql = connection.QueryBuilder($@"select distinct p.ParcelId" 
            );
            sql.AppendLine(SEARCH_FROM);

            switch (query.TransportType)
            {
                case TransportType.Road:
                    AddRoadAdvancedSearchParameters(query, sql);
                    break;
                case TransportType.Air:
                    AddAirAdvancedSearchParameters(query, sql);
                    break;
            }
            AddBranchFilter(sql,allowedBranches);
            var searchResult = await sql.QueryAsync<int>();

            return searchResult;
        }

        public SearchService(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings, IOptionsMonitor<AppSettings> xAppSettings, ILogger<SearchService> logger, IMemoryCache memoryCache) 
            : base(xConnectionSettings,xAppSettings,logger, memoryCache )
        {
        }
    }


}