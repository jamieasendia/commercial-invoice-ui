﻿using System;
using Commercial_Invoice_CodeLib.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Commercial_Invoice_CodeLib.Service
{
    public abstract class ServiceBase<T>
    {
        protected readonly string _connectionString;
        protected readonly int _pageSize;
        protected readonly string _systemApplicationName;
        protected readonly ILogger<T> _logger;
        protected readonly IMemoryCache _memoryCache;
        protected readonly MemoryCacheEntryOptions _memoryCacheEntryOptions;
        protected readonly IOptionsMonitor<AppSettings> _appSettings;
            

        protected ServiceBase(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings,IOptionsMonitor<AppSettings> xAppSettings, ILogger<T> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _memoryCache = memoryCache;
            _connectionString =  _connectionString = xConnectionSettings.CurrentValue.wnConsignDB;
            _pageSize = xAppSettings.CurrentValue.DefaultPageSize;
            _systemApplicationName = xAppSettings.CurrentValue.SystemApplicationName;
            _memoryCacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(3));
            _appSettings = xAppSettings;
        }


    }
}