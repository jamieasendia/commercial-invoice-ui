﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Model
{
    public record SearchQuery
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string Order { get; set; } = "asc";
        //[Required] 
        [DisplayName("Search")]
        public string ParcelConsignmentOrderReference { get; set; }

        public IEnumerable<string> GetSplitParcelConsignmentOrderReferences => string.IsNullOrEmpty(ParcelConsignmentOrderReference)?new List<string>():
            ParcelConsignmentOrderReference.Split(" ", StringSplitOptions.TrimEntries);


        //public AdvancedParameters AdvancedParameters { get; set; }

  
    }
}
