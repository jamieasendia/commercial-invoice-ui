﻿#region Copyright © 2021 Asendia

// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: ISearchService.cs
// Date:     25/02/2021
// Author:   jamie.buchan

#endregion

using System.Collections.Generic;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Model;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface ISearchService
    {
        Task<SearchViewModel> RunSimpleSearch(SearchQuery query, IEnumerable<int> allowedBranches);
        Task<SearchViewModel> RunAdvancedSearch(AdvancedQuery query, IEnumerable<int> allowedBranches);
 
        Task<IEnumerable<int>> GetParcelIdsFromSimpleSearch(SearchQuery query, IEnumerable<int> allowedBranches);
        Task<IEnumerable<int>> GetParcelIdsFromAdvancedSearch(AdvancedQuery query, IEnumerable<int> allowedBranches);
    }
}