﻿namespace Commercial_Invoice_CodeLib.Service
{
    public struct Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string Iso3CountryCode { get; set; }
    }
}